#!/bin/python
import subprocess
import sys

# Constants
NAMESPACE = ""
OBJECT = ""
OBJECT_TYPE = ""


def _get_arguments():
    # Assign user-provided arguments to global variables
    global NAMESPACE
    global OBJECT
    global OBJECT_TYPE

    if len(sys.argv) != 4:
        print("Please pass a Kubernetes object using the following format:\n "
              "./,get-label [NAMESPACE] [OBJECT TYPE] [OBJECT]")
        sys.exit(0)
    # Validate that the argument passed is string, in kubectl cmd
    NAMESPACE = sys.argv[1]
    # Validate pod/po, namespace/ns, replicaset/rs, statefulset/sts, daemonset
    OBJECT_TYPE = sys.argv[2]
    # Validate that the argument passed is string
    OBJECT = sys.argv[3]


def _get_object_label(object_type, namespace, object_k8s):
    # Return label based on user-provided arguments
    output_all = subprocess.check_output(["kubectl", "get", "{}".format(
        object_type), "--namespace={}".format(namespace), "--show-labels"])
    # grep for given object
    index = output_all.find(object_k8s)
    if type(index) is not int:
        print("Couldn't find label for {}".format(object_k8s))
        sys.exit(0)
    line = output_all[index:].split("\n")[0]
    split_by_equals = line.split("=")
    # Final word of first string - word preceding first "=" in line
    key_label = split_by_equals[0].split(" ")[-1]
    # First word of second string - word proceeding first "0" in line up to ","
    value_label = split_by_equals[1].split(",")[0]
    # find ={portion of the object}
    return "{}={}".format(key_label, value_label)


def main():
    """
    Use arguments passed by user when executing program to identify k8s object
    :return: key=value format label for identified k8s object if found
    """
    _get_arguments()
    label = _get_object_label(OBJECT_TYPE, NAMESPACE, OBJECT)
    print("Returned label:\n{}".format(label))


main()
